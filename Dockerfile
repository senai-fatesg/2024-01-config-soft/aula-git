# Usar a imagem oficial do Python como imagem de base
FROM python:3.10-slim

# Definir o diretório de trabalho dentro do contêiner
WORKDIR /app

# Copiar o script hello.py para o diretório de trabalho no contêiner
COPY *.py .

RUN pip install flask


# Executar o script hello.py quando o contêiner iniciar
CMD ["python", "-u", "./app.py"]
