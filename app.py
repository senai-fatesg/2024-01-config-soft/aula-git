from flask import Flask
from datetime import datetime

app = Flask(__name__)

@app.route('/hello', methods=['GET'])
def hello():
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return {'time': current_time, 'message': 'funcionou 123...'}

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


